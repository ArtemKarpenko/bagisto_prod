<?php

namespace Webkul\Color\Repositories;

use Webkul\Core\Eloquent\Repository;

class ColorRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Color\Contracts\Color';
    }
}
<?php

return [
    [
        'key' => 'color',
        'name' => 'Color',
        'route' => 'admin.color.index',
        'sort' => 2
    ]
];
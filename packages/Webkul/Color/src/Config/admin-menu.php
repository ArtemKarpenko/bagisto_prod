<?php

return [
    [
        'key' => 'color',
        'name' => 'Цвета',
        'route' => 'admin.color.index',
        'sort' => 2,
        'icon-class' => 'catalog-icon',
    ]
];
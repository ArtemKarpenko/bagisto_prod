<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColorsProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colors', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор цвета');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('title')->comment('Заголовок цвета');
            $table->string('hex_code')->comment('hex код цвета');
        });

        Schema::create('product_similar_colors', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned();
            $table->integer('child_id')->unsigned();
            $table->foreign('parent_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('child_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('sort')->nullable()->default(0);
        });

        Schema::table('colors', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('colors');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('color_id')->unsigned()->nullable();
            $table->foreign('color_id')->references('id')->on('colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colors');
        Schema::dropIfExists('product_similar_colors');
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('color_id');
        });
    }
}

<?php

namespace Webkul\Admin\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Core\Models\Channel;
use Webkul\Core\Models\Locale;
use Webkul\Product\Facades\ProductImage as ProductImageFacade;
use Webkul\Ui\DataGrid\DataGrid;

class ProductDataGrid extends DataGrid
{
    /**
     * Default sort order of datagrid.
     *
     * @var string
     */
    protected $sortOrder = 'desc';

    protected $hierarchySettings = [
        'status' => false,
        'view_path' => null,
        'function_name' => null,
    ];

    /**
     * Set index columns, ex: id.
     *
     * @var string
     */
    protected $index = 'product_id';

    /**
     * If paginated then value of pagination.
     *
     * @var int
     */
    protected $itemsPerPage = 10;

    /**
     * Locale.
     *
     * @var string
     */
    protected $locale = 'all';

    /**
     * Channel.
     *
     * @var string
     */
    protected $channel = 'all';

    /**
     * Contains the keys for which extra filters to show.
     *
     * @var string[]
     */
    protected $extraFilters = [
        'channels',
        'locales',
    ];

    /**
     * Create datagrid instance.
     *
     * @return void
     */
    public function __construct()
    {
        /* locale */
        $this->locale = core()->getRequestedLocaleCode();

        /* channel */
        $this->channel = core()->getRequestedChannelCode();

        /* parent constructor */
        parent::__construct();
    }

    /**
     * Prepare query builder.
     *
     * @return void
     */
    public function prepareQueryBuilder()
    {
        if ($this->channel === 'all') {
            $whereInChannels = Channel::query()->pluck('code')->toArray();
        } else {
            $whereInChannels = [$this->channel];
        }

        if ($this->locale === 'all') {
            $whereInLocales = Locale::query()->pluck('code')->toArray();
        } else {
            $whereInLocales = [$this->locale];
        }

        /* query builder */
        $queryBuilder = DB::table('product_flat')
            ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
            ->leftJoin('attribute_families', 'products.attribute_family_id', '=', 'attribute_families.id')
            ->leftJoin('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
            ->leftJoin('colors', 'products.color_id', '=', 'colors.id')
            ->select(
                'product_flat.locale',
                'product_flat.channel',
                'product_flat.product_id',
                'products.sku as product_sku',
                'products.id as id',
                'product_flat.product_number',
                'product_flat.name as product_name',
                'products.type as product_type',
                'product_flat.status',
                'product_flat.price',
                'colors.hex_code',
                'attribute_families.name as attribute_family',
                DB::raw('SUM(' . DB::getTablePrefix() . 'product_inventories.qty) as quantity')
            );

        $queryBuilder->groupBy('product_flat.product_id', 'product_flat.locale', 'product_flat.channel');

        $queryBuilder->where('products.parent_id', NULL);

//        $queryBuilder->whereIn('product_flat.locale', $whereInLocales);
//        $queryBuilder->whereIn('product_flat.channel', $whereInChannels);

        $this->addFilter('product_id', 'product_flat.product_id');
        $this->addFilter('product_name', 'product_flat.name');
        $this->addFilter('product_sku', 'products.sku');
        $this->addFilter('product_number', 'product_flat.product_number');
        $this->addFilter('status', 'product_flat.status');
        $this->addFilter('product_type', 'products.type');
        $this->addFilter('attribute_family', 'attribute_families.name');

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * Add columns.
     *
     * @return void
     */
    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
            'closure'    => function ($row) {
                if (!empty($row->product_id) && count($row->variants) && isset($row->product_id)){
                    return '<div class="toggle_childs arrow-down-icon">' . $row->product_id . '</div>';
                }else{
                    return $row->product_id;
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'image',
            'label'      => 'Фото',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                return $this->renderImageView($row);
            },
        ]);

        $this->addColumn([
            'index'      => 'product_name',
            'label'      => trans('admin::app.datagrid.name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'product_sku',
            'label'      => trans('admin::app.datagrid.sku'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => '',
            'label'      => 'Цвет фильтра',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                if(isset($row->hex_code)){
                    return '<div style="border-radius: 5px; width: 20px; height: 20px; background-color: ' . $row->hex_code . ';"></div>';
                }else{
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'boolean',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
            'closure'    => function ($value) {
                if ($value->status == 1) {
                    return trans('admin::app.datagrid.active');
                } else {
                    return trans('admin::app.datagrid.inactive');
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'quantity',
            'label'      => trans('admin::app.datagrid.qty'),
            'type'       => 'number',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                if (is_null($row->quantity)) {
                    return 0;
                } else {
                    return $this->renderQuantityView($row);
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'price',
            'label'      => trans('admin::app.datagrid.price'),
            'type'       => 'price',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
            'closure'    => function ($row) {
                if ($row->product_type == 'configurable'){
                    return $this->renderMinMaxPrice($row);
                }else{
                    return $row->price;
                }

            },
        ]);

        $this->addColumn([
            'index'      => 'categories',
            'label'      => 'Категории',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                return $this->renderCategoriesList($row);
            },
        ]);

//        $this->addColumn([
//            'index'      => 'product_sku',
//            'label'      => trans('admin::app.datagrid.sku'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'product_number',
//            'label'      => trans('admin::app.datagrid.product-number'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'product_name',
//            'label'      => trans('admin::app.datagrid.name'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'attribute_family',
//            'label'      => trans('admin::app.datagrid.attribute-family'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'product_type',
//            'label'      => trans('admin::app.datagrid.type'),
//            'type'       => 'string',
//            'sortable'   => true,
//            'searchable' => true,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'status',
//            'label'      => trans('admin::app.datagrid.status'),
//            'type'       => 'boolean',
//            'sortable'   => true,
//            'searchable' => false,
//            'filterable' => true,
//            'closure'    => function ($value) {
//                if ($value->status == 1) {
//                    return trans('admin::app.datagrid.active');
//                } else {
//                    return trans('admin::app.datagrid.inactive');
//                }
//            },
//        ]);
//
//        $this->addColumn([
//            'index'      => 'price',
//            'label'      => trans('admin::app.datagrid.price'),
//            'type'       => 'price',
//            'sortable'   => true,
//            'searchable' => false,
//            'filterable' => true,
//        ]);
//
//        $this->addColumn([
//            'index'      => 'quantity',
//            'label'      => trans('admin::app.datagrid.qty'),
//            'type'       => 'number',
//            'sortable'   => true,
//            'searchable' => false,
//            'filterable' => false,
//            'closure'    => function ($row) {
//                if (is_null($row->quantity)) {
//                    return 0;
//                } else {
//                    return $this->renderQuantityView($row);
//                }
//            },
//        ]);
    }

    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'title'     => trans('admin::app.datagrid.edit'),
            'method'    => 'GET',
            'route'     => 'admin.catalog.products.edit',
            'icon'      => 'icon pencil-lg-icon',
            'condition' => function () {
                return true;
            },
        ]);

        $this->addAction([
            'title'        => trans('admin::app.datagrid.delete'),
            'method'       => 'POST',
            'route'        => 'admin.catalog.products.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon'         => 'icon trash-icon',
        ]);

        $this->addAction([
            'title'  => trans('admin::app.datagrid.copy'),
            'method' => 'GET',
            'route'  => 'admin.catalog.products.copy',
            'icon'   => 'icon copy-icon',
        ]);
    }

    /**
     * Подготавливаем массив с настройками иерархии.
     *
     * @return void
     */
    public function prepareHierarchySettings()
    {
        $this->hierarchySettings = [
            'status' => true,
            'view_name' => 'product_variants',
            'function_name' => 'addProductVariants',
        ];
    }

    /**
     * Prepare mass actions.
     *
     * @return void
     */
    public function prepareMassActions()
    {
        $this->addMassAction([
            'type'   => 'delete',
            'label'  => trans('admin::app.datagrid.delete'),
            'action' => route('admin.catalog.products.massdelete'),
            'method' => 'POST',
        ]);

        $this->addMassAction([
            'type'    => 'update',
            'label'   => trans('admin::app.datagrid.update-status'),
            'action'  => route('admin.catalog.products.massupdate'),
            'method'  => 'POST',
            'options' => [
                trans('admin::app.datagrid.active')    => 1,
                trans('admin::app.datagrid.inactive')  => 0,
            ],
        ]);
    }

    /**
     * Render quantity view.
     *
     * @param  object  $row
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    private function renderQuantityView($row)
    {
        $product = app(\Webkul\Product\Repositories\ProductRepository::class)->find($row->product_id);

        $inventorySources = app(\Webkul\Inventory\Repositories\InventorySourceRepository::class)->findWhere(['status' => 1]);

        $totalQuantity = $row->quantity;

        return view('admin::catalog.products.datagrid.quantity', compact('product', 'inventorySources', 'totalQuantity'))->render();
    }

    private function renderImageView($row)
    {
        $product = app(\Webkul\Product\Repositories\ProductRepository::class)->find($row->product_id);

        $images = ProductImageFacade::getProductBaseImage($product);

        $image = count($images) ? $images['small_image_url'] : null;

        $id = $row->product_id;

        return view('admin::catalog.products.datagrid.image', compact('image', 'id'))->render();
    }

    private function renderMinMaxPrice($row)
    {
        $product = app(\Webkul\Product\Repositories\ProductRepository::class)->find($row->product_id);

        $prices = $product->getTypeInstance()->getMinMaxPrice();

        return $prices['min'] . ' - ' . $prices['max'];
    }

    private function renderCategoriesList($row)
    {
        $product = app(\Webkul\Product\Repositories\ProductRepository::class)->find($row->product_id);

        $categories = $product->categories()->get();

        $allCategoriesNamesArray = [];
        foreach ($categories as $category) {
            $allCategoriesNamesArray[] = $category->translate($this->locale)['name'];
        }

        return implode(', ', $allCategoriesNamesArray);
    }
}

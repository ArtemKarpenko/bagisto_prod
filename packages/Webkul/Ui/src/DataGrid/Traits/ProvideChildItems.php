<?php

namespace Webkul\Ui\DataGrid\Traits;

use Illuminate\Support\Facades\DB;

trait ProvideChildItems
{
    /**
     * Добавляем к дате дочерние варианты.
     *
     * @param  array  $data
     * @return array
     */
    private function addProductVariants($data)
    {
        foreach ($data as $item) {
            $item->variants = DB::table('product_flat')
                ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
                ->leftJoin('attribute_families', 'products.attribute_family_id', '=', 'attribute_families.id')
                ->leftJoin('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->leftJoin('product_attribute_values', 'product_attribute_values.product_id', '=', 'products.id')
                ->leftJoin('attributes', 'attributes.id', '=', 'product_attribute_values.attribute_id')
                ->leftJoin('attribute_options', 'attribute_options.id', '=', 'product_attribute_values.integer_value')
                ->select(
                    'product_flat.locale',
                    'product_flat.channel',
                    'product_flat.product_id',
                    'products.sku as product_sku',
                    'product_flat.product_number',
                    'product_flat.name as product_name',
                    'products.type as product_type',
                    'product_flat.status',
                    'product_flat.price',
                    'attribute_options.admin_name as size',
                    'attribute_families.name as attribute_family',
                    DB::raw('SUM(' . DB::getTablePrefix() . 'product_inventories.qty) as quantity')
                )
                ->where('attributes.code', 'size')
                ->where('products.parent_id', $item->product_id)
                ->groupBy('product_flat.product_id', 'product_flat.locale', 'product_flat.channel')
                ->get();
        }
        return $data;
    }
}
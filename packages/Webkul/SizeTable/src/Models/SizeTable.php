<?php

namespace Webkul\SizeTable\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\SizeTable\Contracts\SizeTable as SizeTableContract;

class SizeTable extends Model implements SizeTableContract
{
    protected $fillable = ['title'];
}
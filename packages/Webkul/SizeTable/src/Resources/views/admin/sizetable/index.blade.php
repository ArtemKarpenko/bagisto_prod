@extends('admin::layouts.content')

@section('page_title')
    Все Размерные таблицы
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Размерные таблицы</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.sizetable.create') }}" class="btn btn-lg btn-primary">
                    Добавить размерную таблицу
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('sizeTableGroup','Webkul\SizeTable\DataGrids\SizeTableDataGrid')
            {!! $sizeTableGroup->render() !!}
        </div>
    </div>

@stop
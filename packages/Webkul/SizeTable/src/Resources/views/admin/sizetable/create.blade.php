@extends('admin::layouts.content')

@section('page_title')
    Добавить размерную таблицу
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.sizetable.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.sizetable.index') }}'"></i>

                        Добавить размерную таблицу
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить размерную таблицу
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="title" v-validate="'required'" value="{{ old('title') }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>
                    </div>

                    <div class="control-group">
                        <label>Изображение размерной таблицы</label>

                        @if (isset($sizetable) && $sizetable->image)
                            <image-wrapper
                                :multiple="false"
                                input-name="image"
                                :images='"{{ url()->to('/') . '/storage/' . $sizetable->image }}"'
                                :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'">
                            </image-wrapper>
                        @else
                            <image-wrapper
                                :multiple="false"
                                input-name="image"
                                :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'">
                            </image-wrapper>
                        @endif

                    </div>

                </div>
            </div>

        </form>
    </div>
@stop
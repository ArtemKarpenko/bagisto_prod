<?php

namespace Webkul\SizeTable\Repositories;

use Illuminate\Support\Facades\Storage;
use Webkul\Core\Eloquent\Repository;

class SizeTableRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\SizeTable\Contracts\SizeTable';
    }

    /**
     * @param  array  $data
     * @return \Webkul\SizeTable\Contracts\SizeTable
     */
    public function create(array $data)
    {
        $sizetable = $this->model->create($data);

        $this->uploadImages($data, $sizetable);

        return $sizetable;
    }

    /**
     * @param  array  $data
     * @param  int  $id
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $sizetable = $this->find($id);

        $sizetable->update($data);

        $this->uploadImages($data, $sizetable);

        return $sizetable;
    }

    public function uploadImages($data, $sizetable, $type = "image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'sizetable/' . $sizetable->id;

                if ($request->hasFile($file)) {
                    if ($sizetable->{$type}) {
                        Storage::delete($sizetable->{$type});
                    }

                    $sizetable->{$type} = $request->file($file)->store($dir);
                    $sizetable->save();
                }
            }
        } else {
            if ($sizetable->{$type}) {
                Storage::delete($sizetable->{$type});
            }

            $sizetable->{$type} = null;
            $sizetable->save();
        }
    }
}
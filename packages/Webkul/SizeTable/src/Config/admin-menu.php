<?php

return [
    [
        'key' => 'sizetable',
        'name' => 'Размерные таблицы',
        'route' => 'admin.sizetable.index',
        'sort' => 2,
        'icon-class' => 'catalog-icon',
    ]
];
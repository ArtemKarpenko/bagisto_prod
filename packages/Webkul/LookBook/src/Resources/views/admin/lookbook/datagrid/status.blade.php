<span id="coupon-{{ $lookbook->id }}-status">
   <div class="control-group">
        <label class="switch" style="margin-top: 0px; margin-bottom: 0px;">
            <input type="checkbox" id="status" name="status" value="{{ $lookbook->status }}" {{ $lookbook->status ? 'checked' : '' }}>
            <span class="slider round" onclick="changeLookBookStatus('{{ route('admin.lookbook.update-status', $lookbook->id) }}', {{ $lookbook->id }})"></span>
        </label>
    </div>
</span>


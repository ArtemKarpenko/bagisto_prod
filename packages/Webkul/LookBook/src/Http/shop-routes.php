<?php

Route::group([
        'prefix'     => 'lookbook',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'Webkul\LookBook\Http\Controllers\Shop\LookBookController@index')->defaults('_config', [
            'view' => 'lookbook::shop.index',
        ])->name('shop.lookbook.index');

});
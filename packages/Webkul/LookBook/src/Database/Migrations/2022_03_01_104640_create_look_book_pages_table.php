<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookBookPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('look_book_pages', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор страницы лукбука');
            $table->integer('look_book_id')->unsigned()->comment('Идентификатор лукбука, к которому привязана данная страница');
            $table->smallInteger('status')->default(0)->comment('Статус страницы лукбука');
            $table->string('name')->comment('Наименование страницы лукбука');
            $table->string('color')->comment('Цвет страницы лукбука');
            $table->integer('sort')->default(0)->comment('Порядок сортировки страницы лукбука');
            $table->string('image')->nullable()->comment('Изображение страницы лукбука');
            $table->timestamps();
            $table->foreign('look_book_id')->references('id')->on('look_books')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('look_book_pages');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('look_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Наименование лукбука');
            $table->text('description')->nullable()->comment('Описание лукбука');
            $table->boolean('status')->default(0)->comment('Статус видимости лукбука');
            $table->string('image_general')->nullable()->comment('Основная обложка');
            $table->string('image_main_1')->nullable()->comment('Обл. для главной-1');
            $table->string('image_main_2')->nullable()->comment('Обл. для главной-2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('look_books');
    }
}

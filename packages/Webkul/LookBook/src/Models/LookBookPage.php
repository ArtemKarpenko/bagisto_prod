<?php

namespace Webkul\LookBook\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\LookBook\Contracts\LookBookPage as LookBookPageContract;
use Webkul\LookBook\Models\LookBookPageItemProxy;

class LookBookPage extends Model implements LookBookPageContract
{
    protected $table = 'look_book_pages';
    protected $fillable = ['id', 'name', 'look_book_id', 'sort'];

    /**
     * The products that belong to the Look book page.
     */
    public function products()
    {
        return $this->hasMany(LookBookPageItemProxy::modelClass(), 'parent_id');
    }
}
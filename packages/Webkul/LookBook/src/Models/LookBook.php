<?php

namespace Webkul\LookBook\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Webkul\LookBook\Contracts\LookBook as LookBookContract;
use Webkul\LookBook\Models\LookBookPage;

class LookBook extends Model implements LookBookContract
{
    protected $table = 'look_books';
    protected $fillable = ['id', 'name', 'description', 'status'];

    /**
     * The Look Book Pages that belong to the Look Book.
     */
    public function pages()
    {
        return $this->hasMany(LookBookPageProxy::modelClass(), 'look_book_id')->orderBy('sort', 'asc');
    }

    /**
     * Получить URL для Основной обложки.
     */
    public function image_general_url()
    {
        if (! $this->image_general) {
            return;
        }

        return Storage::url($this->image_general);
    }

    /**
     * Получить URL для Обл. для главной-1.
     */
    public function image_main_1_url()
    {
        if (! $this->image_main_1) {
            return;
        }

        return Storage::url($this->image_main_1);
    }

    /**
     * Получить URL для Обл. для главной-2.
     */
    public function image_main_2_url()
    {
        if (! $this->image_main_2) {
            return;
        }

        return Storage::url($this->image_main_2);
    }
}
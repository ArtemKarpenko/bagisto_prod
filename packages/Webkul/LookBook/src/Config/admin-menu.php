<?php

return [
    [
        'key' => 'lookbook',
        'name' => 'Лукбуки',
        'route' => 'admin.lookbook.index',
        'sort' => 2,
        'icon-class' => 'catalog-icon',
    ]
];
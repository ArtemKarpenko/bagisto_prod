<?php

namespace Webkul\LookBook\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Webkul\LookBook\Models\LookBook::class,
        \Webkul\LookBook\Models\LookBookPage::class,
        \Webkul\LookBook\Models\LookBookPageItem::class,
    ];
}
<?php

namespace Webkul\HomeCategory\Models;

use Konekt\Concord\Proxies\ModelProxy;

class HomeCategoryProxy extends ModelProxy
{

    public function targetClass(): string
    {
        return HomeCategory::class;
    }

}
<?php

namespace Webkul\HomeCategory\Models;

use Webkul\Core\Eloquent\TranslatableModel;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Webkul\HomeCategory\Contracts\HomeCategory as HomeCategoryContract;
use Webkul\Attribute\Models\AttributeProxy;
use Webkul\HomeCategory\Repositories\HomeCategoryRepository;
use Webkul\Product\Models\ProductProxy;

/**
 * Class HomeCategory
 *
 * @package Webkul\HomeCategory\Models
 *
 * @property-read string $url_path maintained by database triggers
 */
class HomeCategory extends TranslatableModel implements HomeCategoryContract
{
    use NodeTrait;

    public $translatedAttributes = [
        'name',
        'description',
        'slug',
        'url_path',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    protected $fillable = [
        'position',
        'status',
        'display_mode',
        'parent_id',
        'additional',
    ];

    protected $with = ['translations'];

    /**
     * Get image url for the category image.
     */
    public function image_url()
    {
        if (! $this->image)
            return;

        return Storage::url($this->image);
    }

    /**
     * Get image url for the category image.
     */
    public function getImageUrlAttribute()
    {
        return $this->image_url();
    }

     /**
     * The filterable attributes that belong to the category.
     */
    public function filterableAttributes()
    {
        return $this->belongsToMany(AttributeProxy::modelClass(), 'home_category_filterable_attributes')->with(['options' => function($query) {
            $query->orderBy('sort_order');
        }]);
    }

    /**
     * Getting the root category of a category
     *
     * @return HomeCategory
     */
    public function getRootCategory(): Category
    {
        return HomeCategory::where([
            ['parent_id', '=', null],
            ['_lft', '<=', $this->_lft],
            ['_rgt', '>=', $this->_rgt],
        ])->first();
    }

    /**
     * Returns all categories within the category's path
     *
     * @return HomeCategory[]
     */
    public function getPathCategories(): array
    {
        $category = $this->findInTree();

        $categories = [$category];

        while (isset($category->parent)) {
            $category = $category->parent;
            $categories[] = $category;
        }

        return array_reverse($categories);
    }

    /**
     * Finds and returns the category within a nested category tree
     * will search in root category by default
     * is used to minimize the numbers of sql queries for it only uses the already cached tree
     *
     * @param HomeCategory[] $categoryTree
     * @return HomeCategory
     */
    public function findInTree($categoryTree = null): HomeCategory
    {
        if (! $categoryTree) {
            $categoryTree = app(HomeCategoryRepository::class)->getVisibleCategoryTree($this->getRootCategory()->id);
        }

        $category = $categoryTree->first();

        if (! $category) {
            throw new NotFoundHttpException('category not found in tree');
        }

        if ($category->id === $this->id) {
            return $category;
        }

        return $this->findInTree($category->children);
    }

    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->belongsToMany(ProductProxy::modelClass(), 'product_home_categories');
    }
}
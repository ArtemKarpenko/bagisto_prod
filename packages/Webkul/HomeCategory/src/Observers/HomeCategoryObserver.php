<?php

namespace Webkul\HomeCategory\Observers;

use Illuminate\Support\Facades\Storage;
use Webkul\HomeCategory\Models\HomeCategory;
use Carbon\Carbon;

class HomeCategoryObserver
{
    /**
     * Handle the Category "deleted" event.
     *
     * @param  \Webkul\HomeCategory\Contracts\HomeCategory  $category
     * @return void
     */
    public function deleted($category)
    {
        Storage::deleteDirectory('category/' . $category->id);
    }

    /**
     * Handle the Category "saved" event.
     *
     * @param  \Webkul\HomeCategory\Contracts\HomeCategory  $category
     */
    public function saved($category)
    {
        foreach ($category->children as $child) {
            $child->touch();
        }
    }
}
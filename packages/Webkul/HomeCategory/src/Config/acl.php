<?php

return [
    [
        'key' => 'homecategory',
        'name' => 'HomeCategory',
        'route' => 'admin.homecategory.index',
        'sort' => 2
    ]
];
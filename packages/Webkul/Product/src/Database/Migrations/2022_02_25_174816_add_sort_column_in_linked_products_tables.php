<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSortColumnInLinkedProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_up_sells', function (Blueprint $table) {
            $table->integer('sort')->nullable()->default(0);
        });
        Schema::table('product_relations', function (Blueprint $table) {
            $table->integer('sort')->nullable()->default(0);
        });
        Schema::table('product_cross_sells', function (Blueprint $table) {
            $table->integer('sort')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_up_sells', function (Blueprint $table) {
            $table->dropColumn('sort');
        });
        Schema::table('product_relations', function (Blueprint $table) {
            $table->dropColumn('sort');
        });
        Schema::table('product_cross_sells', function (Blueprint $table) {
            $table->dropColumn('sort');
        });
    }
}

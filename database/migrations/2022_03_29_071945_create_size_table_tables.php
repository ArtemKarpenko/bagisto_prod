<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSizeTableTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('size_tables', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор размерной таблицы');
            $table->string('title')->comment('Заголовок размерной таблицы');
            $table->string('image')->nullable()->comment('Изображение размерной таблицы');
            $table->timestamps();
        });

        Schema::create('product_size_tables', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('size_table_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('size_table_id')->references('id')->on('size_tables')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('size_tables');
        Schema::dropIfExists('product_size_tables');
    }
}
